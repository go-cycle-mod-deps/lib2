package lib2

import (
	"fmt"

	"gitlab.com/go-cycle-mod-deps/lib1"
)

func Print(v lib1.Lib1Type) {
	fmt.Printf("%f %s\n", v.Float, v.String)
}

func Func() {
	lib1.Func()
}

func Func2() {
	lib1.Func2()
}
